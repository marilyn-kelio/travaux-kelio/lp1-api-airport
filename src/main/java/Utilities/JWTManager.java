package Utilities;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.io.UnsupportedEncodingException;
import java.security.Key;

import java.util.Date;

public class JWTManager {
    public void createJWTToken() {

        Date expirationDate = new Date();
        expirationDate.setTime(new Date().getTime() + 86400000);

        try {
            String jwt = Jwts.builder()
                    .setSubject("users/TzMUocMF4p")
                    .setExpiration(expirationDate)
                    .claim("name", "Robert Token Man")
                    .claim("scope", "self groups/admins")
                    .signWith(
                        Keys.secretKeyFor(SignatureAlgorithm.HS256)
                    )
                    .compact();
        } catch (Exception e) {
            e.printStackTrace();
            LogManager.WriteLog(e.getMessage());
        }
    }
}
