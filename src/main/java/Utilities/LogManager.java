package Utilities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LogManager {

    private static final String logFileName = "D:\\Code\\Projets cours\\LP1\\lp1-api-airport\\Logs\\Log.txt";
    private static final java.time.format.DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    /**
     * Writes a log message in the log file
     * @param message
     */
    public static void WriteLog(String message) {
        try {
            // Création de l'instance écrivant le contenu
            BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true));

            // Ajout du contenu du message au fichier
            writer.append(dtf.format(LocalDateTime.now()) + " : " + message + "\n");

            // Fermeture de l'instance
            writer.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
