package Utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.sql.*;
import java.util.ArrayList;

public abstract class ConnectionToDB {

    private final static String url = "jdbc:mariadb://mysql.iutrs.unistra.fr/ksteiner";
    private final static String username = "ksteiner";
    private final static String password = "mdpPhpLP1";

    private final static ObjectWriter objWrit = new ObjectMapper().writer().withDefaultPrettyPrinter();

    /**
     * Opens a connection to the database, execute the request in parameter and returns the value, then closes the connection.
     * @param sqlRequest
     * @return Json formatted sql request response
     */
    public static ResultSet GetDataFromDb(String sqlRequest) {

        // Réalisation de la requête à la DB et gestion du résultat
        ResultSet result = HandleConnectionToDB(sqlRequest);

        // CloseRequest(result); => Non fonctionnel, aucune idée de pourquoi, impossible de lancer le débogage

        return result;
    }


    private static ArrayList<String> ConvertGetRequestResultToString(ResultSet result) {

        // Création du conteneur des résultats de la requête
        ArrayList<String> resultArray = new ArrayList<>();

        try {
            while (result.next()) {
                ResultSetMetaData metaData = result.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    resultArray.add(metaData.getColumnName(i) + " : " + result.getString(i));
                }
            }

            return resultArray;
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }

        return null;
    }

    /**
     * Opens a connection with the DataBase
     * @return Instance of the open connection
     */
    private static ResultSet HandleConnectionToDB(String slqRequest) {

        // Tentative de connection
        try {
            // Renseigne la classe du driver à utiliser
            Class.forName("org.mariadb.jdbc.Driver");

            // Ouvre la connection avec la base de données
            Connection connection = DriverManager.getConnection(url, username, password);

            // Exécution de la prochaine étape de connection à la DB
            return PrepareStatement(connection, slqRequest);
        }
        catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }

        return null;
    }

    /**
     * Prepare a sql request
     * @param connection
     * @param sqlRequest
     * @return Instance of the prepared request
     */
    private static ResultSet PrepareStatement(Connection connection, String sqlRequest) {
        try {
            // Tentaive de préparation de la requête sql
            PreparedStatement preparedStatement = connection.prepareStatement(sqlRequest);

            // Exécution de la prochaine étape de connection à la DB
            return ExecuteStatementQuery(preparedStatement);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }

        return null;
    }

    /**
     * Execute the query of a prepared statement
     * @param statement
     * @return Instance of the result of the request
     */
    private static ResultSet ExecuteStatementQuery(PreparedStatement statement) {
        try {
            // Tentaive d'exécution de la requête sql
            return statement.executeQuery();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }

        return null;
    }

    /**
     * Close a connection if it still is active
     * @param connection
     */
    private static void CloseConnection(Connection connection) {
        try {
            // Si la connexion est toujours active
            if (!connection.isClosed()) {

                // Ferme la connexion
                connection.close();
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }
    }

    /**
     * Close a statement if it still is active
     * @param statement
     */
    private static void CloseStatement(Statement statement) {
        try {
            // Si la connexion est toujours active
            if (!statement.isClosed()) {

                // Ferme la connexion
                statement.close();
            }

            CloseConnection(statement.getConnection());
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }
    }

    /**
     * Close a result if it still is active
     * @param result
     */
    private static void CloseRequest(ResultSet result) {
        try {
            // Si la connexion est toujours active
            if (!result.isClosed()) {

                // Ferme la connexion
                result.close();
            }

            CloseStatement(result.getStatement());
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }
    }
}
