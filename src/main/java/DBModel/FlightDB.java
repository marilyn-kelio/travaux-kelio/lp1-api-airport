package DBModel;

import Model.Flight;
import Utilities.ConnectionToDB;
import Utilities.LogManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class FlightDB extends ConnectionToDB {

    private static List<Flight> flights = new ArrayList<>();

    /**
     * Request flights to the DB and actualize the list
     */
    public static void requestFlightsFromDB() {
        ResultSet result = GetDataFromDb(Flight.getRequestSyntax());

        try{
            while (result.next()) {
                flights.add(new Flight(
                    result.getString("numVol"),
                    result.getString("villeDepart"),
                    result.getString("villeArrivee"),
                    result.getInt("heureDepart")
                ));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            LogManager.WriteLog(ex.getMessage());
        }
    }

    public static List<Flight> getFlights() { return flights; }
}
