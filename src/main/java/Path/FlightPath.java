package Path;

import DBModel.FlightDB;
import Model.Flight;
import Utilities.LogManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.ws.rs.*;
import java.util.ArrayList;

@Path("/flights")
public class FlightPath {

    private final ObjectWriter objWrit = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @GET
    public String getFlights() {

        // Récupération des vols depuis la base de données
        FlightDB.requestFlightsFromDB();

        // Tentative de renvoi des vols convertis en JSON
        try {
            return objWrit.writeValueAsString(FlightDB.getFlights());
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }

    }

    @GET
    @Path("numvol-{id}")
    public String getSpecificFlight(@PathParam("id") String id) {

        // Récupération des vols depuis la base de données
        FlightDB.requestFlightsFromDB();

        Flight selectedFlight = new Flight();

        // Sélection du vol selon son id
        for(Flight flight : FlightDB.getFlights()) {
            if (flight.getNumVol().equals(id)) {
                selectedFlight = flight;
            }
        }

        // Tentative de renvoi des vols convertis en JSON
        try {
            return objWrit.writeValueAsString(selectedFlight);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }

    @GET
    @Path("search")
    public String getFlightBySearch(@QueryParam("departure") String depart, @QueryParam("arrival") String arrival, @QueryParam("departureTime") int departTime) {

        // Récupération des vols depuis la base de données
        FlightDB.requestFlightsFromDB();

        ArrayList<Flight> selectedFlight = new ArrayList<>();

        // Ajout des vols concernés par la recherche dans la liste
        for(Flight flight : FlightDB.getFlights()) {
            if (flight.getVilleDepart().equals(depart) && flight.getVilleArrivee().equals(arrival) && flight.getHeureDepart() == departTime) {
                selectedFlight.add(flight);
            }
        }

        // Tentative de renvoi des vols convertis en JSON
        try {
            return objWrit.writeValueAsString(selectedFlight);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }

    /* Route plus utilisée et incompatible avec classe ConnectionToDB actuelle
    @GET
    @Path("test-db")
    public String testDbConnection() {
        ArrayList<String> resultList = ConnectionToDB.GetDataFromDb("SELECT * FROM Test");
        String message = "";

        for (String result : resultList) {
            message += result + "\n";
        }
        return message;
    }*/

    @POST
    @Path("test-log")
    public void testLog() {
        LogManager.WriteLog("test de log");
    }
}
